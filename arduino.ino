#include <Stepper.h> 

// Declarações e Inicializações do sistema de comunicação Bluetooth.
String readBluetooth; 
int count_food_sts = 8;
int lamp=13;//LED da porta 13
unsigned long time_hour = 5000;
unsigned long last_time_feed = 0;
unsigned int hours_last_feed = 0;
bool automatic = false;
bool automatic_feed = false;
unsigned int automatic_hours_feed = 0;
unsigned short int wrote = 0;

// Declarações e inicializações do sistema de controle de movimento.
#define PIN_BUTTON_UP                 2
#define PIN_BUTTON_DOWN               3
#define PIN_STEPPER_IN1               4
#define PIN_STEPPER_IN2               5
#define PIN_STEPPER_IN3               6
#define PIN_STEPPER_IN4               7

#define ALIGNMENT_LOW_SPEED_DELAY     500
#define ALIGNMENT_HIGH_SPEED_DELAY    100
#define ALIGNMENT_LOW_SPEED_DURATION  3000
#define DEFAULT_ROTATION_SPEED        60
#define STEPS_AMOUNT_PER_REVOLUTION   500
#define STEPS_AMOUNT_90_DEGREES       512

unsigned upButtonPressTime;
unsigned downButtonPressTime;
unsigned alignmentDelay;

//Inicializa a biblioteca
Stepper stepper(STEPS_AMOUNT_PER_REVOLUTION, 
                PIN_STEPPER_IN1, 
                PIN_STEPPER_IN3, 
                PIN_STEPPER_IN2, 
                PIN_STEPPER_IN4); 


void setup()
{
  pinMode(lamp,OUTPUT);//Definindo o pino 13 como saída
  pinMode(PIN_BUTTON_UP, INPUT);
  pinMode(PIN_BUTTON_DOWN, INPUT);
  
  //Determina a velocidade inicial do motor 
  stepper.setSpeed(DEFAULT_ROTATION_SPEED);
  
  upButtonPressTime = 0;
  downButtonPressTime = 0;
  alignmentDelay = ALIGNMENT_LOW_SPEED_DELAY;
  
  Serial.begin(9600);//Inicia comunicação serial
}
 
void loop()
{

  //Alinha as pás
  while ((digitalRead(PIN_BUTTON_UP) == false) || (digitalRead(PIN_BUTTON_DOWN) == false))
  {
    if (digitalRead(PIN_BUTTON_UP) == false)
    {
      if (upButtonPressTime > ALIGNMENT_LOW_SPEED_DURATION) { alignmentDelay = ALIGNMENT_HIGH_SPEED_DELAY; }
      else { alignmentDelay = ALIGNMENT_LOW_SPEED_DELAY; }
      
      stepper.step(-10);

      downButtonPressTime = 0;
      upButtonPressTime += alignmentDelay;
      delay(alignmentDelay);
    }
    else { upButtonPressTime = 0; }

    if (digitalRead(PIN_BUTTON_DOWN) == false)
    {
      if (downButtonPressTime > ALIGNMENT_LOW_SPEED_DURATION) { alignmentDelay = ALIGNMENT_HIGH_SPEED_DELAY; }
      else { alignmentDelay = ALIGNMENT_LOW_SPEED_DELAY; }
      
      stepper.step(10);
      
      upButtonPressTime = 0;
      downButtonPressTime += alignmentDelay;
      delay(alignmentDelay);
    }
    else { downButtonPressTime = 0; }
  }

  if(automatic)
  {
    if(automatic_feed && count_food_sts > 0)
    {
      feedFish();
      automatic_feed = false;
    }
    else if(count_food_sts <= 0)
    {
      count_food_sts = 0;
      automatic_hours_feed = 0;
      automatic = false;
      automatic_feed = false;
    }
  }
  
  if(Serial.available())
  {
    
    readBluetooth = Serial.readString();
    //int readIntBluetooth = Serial.readString().toInt();
    
    if(readBluetooth.equals("F")) // Se usuário clicou em "Feed"
    {
      Serial.println("Feed");      
      feedFish();    
    }
    
    else if(readBluetooth.equals("R") && !automatic) // Se usuário clicou em "Refill" e não está no automático
      resetCounter();   

    else if(readBluetooth.equals("S")) // Se usuário clicou conectou Bluetooth
      writeGenSts();
      
    else
    {
      Serial.print(readBluetooth);
      automatic = true;
      automatic_hours_feed = readBluetooth.toInt();
      //count_food_sts -= 1;
      feedFish(); 
    }  
 }  
 if(count_food_sts < 0)
  count_food_sts = 0;
 
 writeFoodSts();
 checkLastFeed(); 

}

void feedFish()
{
  //Gira o motor no sentido horario a 90 graus
  stepper.step(-STEPS_AMOUNT_90_DEGREES); 
  delay(200);

  // Atualiza os contadores.
  count_food_sts -= 1;
  hours_last_feed = 0;
  last_time_feed = millis();
}

void writeFoodSts()
{
  if(count_food_sts == 8 and wrote == 0)
  {
    //Serial.println("Full");
    Serial.write("FL");
    wrote = 1;
  }
  else if(count_food_sts == 4 and wrote == 1)
  {
    //Serial.println("Half Empty");
    Serial.write("HF");
    wrote = 2;
  }
  else if(count_food_sts == 0 and wrote == 2)
  {
    //Serial.println("Empty");
    Serial.write("EP");
    wrote = 0;
  }
}

void writeGenSts()
{
  delay(100);
  if(count_food_sts <= 8 && count_food_sts > 4)
    Serial.write("FL");
  else if(count_food_sts <= 4 && count_food_sts > 0)
    Serial.write("HF");
  else if(count_food_sts == 0)
    Serial.write("EP");
    
  delay(500);
  Serial.print(hours_last_feed);
  Serial.print(" Hrs");
  
}

void resetCounter()
{
  count_food_sts = 8; 
}

void checkLastFeed()
{
  int hours = 0;
  unsigned long time = millis();
  if(time - last_time_feed >= time_hour)
  {
    hours_last_feed = (time - last_time_feed)/time_hour;
    
    if(automatic && hours_last_feed >= automatic_hours_feed)
    {
      hours_last_feed = 0;
      automatic_feed = true;
    }
    Serial.print(hours_last_feed);
    Serial.print(" Hrs");      
  }
}
